### Global NPMS
sudo npm install bower gulp forever -g

### Grab the front end repo
sudo git clone git@gitlab.com:blox/angular_gulp_seed.git /shared
sudo cd /shared/angular_gulp_seed
sudo npm install
sudo bower install -f

### Grab the API repo
sudo git clone git@gitlab.com:blox/hapi_api_seed.git /shared
sudo cd /shared/hapi_api_seed
sudo npm install
sudo forever start /shared/hapi_api_seed/index.js

