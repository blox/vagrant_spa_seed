### GITLAB
### Remove old key
if [ -e "/shared/gitlab_public_key_id.txt" ]; then
  export GITLAB_KEY_ID=$(sudo cat /shared/gitlab_public_key_id.txt)
  sudo curl -X DELETE -H "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v3/user/keys/${GITLAB_KEY_ID}"
fi

### Add Public key to Gitlab
sudo rm -f /vagrant/gitlab_public_key_id.txt
sudo curl -X POST -H "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -H "Content-type: application/json" --data '{"title":"'"${GITLAB_KEY_NAME}"'","key":"'"${KEY_VALUE}"'"}' "https://gitlab.com/api/v3/user/keys" | jq .id >> /vagrant/gitlab_public_key_id.txt

### Add Gitlab to the list of known_hosts
sudo ssh-keyscan -H gitlab.com >> /root/.ssh/known_hosts
sudo chmod 600 /root/.ssh/known_hosts