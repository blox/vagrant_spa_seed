### Install git
sudo apt-get install -y git

### Generate ssh keys
sudo ssh-keygen -t rsa -f /root/.ssh/id_rsa -q -N ""

### Remove any old keys
sudo rm -f /shared/id_rsa.txt

### Copy new generated key
sudo cat /root/.ssh/id_rsa.pub >> /shared/id_rsa.txt

### jq for object manipulation
sudo apt-get install -y jq

### SH Variables
export KEY_VALUE=`cat /root/.ssh/id_rsa.pub`

### Git config
git config --global user.name "${GIT_NAME}"
git config --global user.email "${GIT_EMAIL}"
git config --global core.autocrlf false