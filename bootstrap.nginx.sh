### Get the nginx repo ppa
sudo add-apt-repository -y ppa:nginx/stable
sudo apt-get update
sudo apt-get install -y nginx

### Set up configs
sudo rm -f '/etc/nginx/nginx.conf'
sudo cp '/shared/nginx.conf' '/etc/nginx'
sudo rm -f '/etc/nginx/sites-available/default'
sudo rm -f '/etc/nginx/sites-enabled/default'
sudo cp '/shared/playground1.com.conf' '/etc/nginx/sites-available'
sudo ln -s '/etc/nginx/sites-available/playground1.com.conf' '/etc/nginx/sites-enabled'
sudo service nginx restart